from django.shortcuts import render
from datetime import datetime, timedelta
from django.utils import timezone

# Create your views here.
def datetime(request):
    context = {'now' : timezone.now()}
    return render(request, 'datetimepage-templates/datetimepage.html', context)

def generatedDatetime(request, hours=0):
    context = {'now' : timezone.now(), 'addedtime' : timezone.now() + timedelta(hours=hours)}
    return render(request, 'datetimepage-templates/generatedDatetime.html', context)