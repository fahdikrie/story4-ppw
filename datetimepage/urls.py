from django.urls import path
from . import views

urlpatterns = [
    path('', views.datetime, name='datetime'),
    path('<int:hours>/', views.generatedDatetime, name='generatedDatetime'),
]