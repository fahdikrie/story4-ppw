from django.shortcuts import render

# Create your views here.
def index(request):
    return render(request, 'homepage-templates/homepage.html')

def story(request):
    return render(request, 'homepage-templates/story3.html')