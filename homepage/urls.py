from django.urls import path
from . import views

urlpatterns = [
    path('', views.index, name='homepage'),
    path('story/', views.story, name='story3'),
]