from django.shortcuts import render, redirect, get_object_or_404
from django.urls import reverse
from .models import Events, EventGuests
from .forms import EventsForm, EventGuestsForm

# Create your views here.
def events(request):
    event = Events.objects.all()
    guests = EventGuests.objects.all()

    if request.method == 'POST':
        form = EventsForm(request.POST)
        if form.is_valid():
            form.save() # versi 1
            # models.EventsForm(**form.cleaned_data).save() # versi 2
            form = EventsForm()

    else:
        form = EventsForm()

    return render(request, 'eventpage-templates/events.html', {'form': form, 'event' : event, 'guests' : guests})

def eventsGuestForm(request, nama_event):
    event = get_object_or_404(Events, nama_event=nama_event)

    if request.method == 'POST':
        guestform = EventGuestsForm(request.POST)
        if guestform.is_valid():
            guest = guestform.save(commit=False)
            guest.event_host = event
            guest.save()

            # guestform.event_host = event
            # guestform.save() # versi 1
            # models.EventsForm(**form.cleaned_data).save() # versi 2
            # guestform = EventsGuestForm()
            return redirect('/event/')

    else:
        guestform = EventGuestsForm()

    return render(request, 'eventpage-templates/eventsguestform.html', {'event' : event, 'guestform' : guestform})



def deleteEvent(request):
    if request.method == 'POST' and 'id' in request.POST:
        Events.objects.get(id=request.POST['id']).delete()

    return redirect(reverse('event'))



    # def events(request):
    # model = Events.objects.all()
    # guests = EventGuests.objects.all()

    # if request.method == 'POST':
    #     form = EventsForm(request.POST)
    #     guestform = EventGuestsForm(request.POST)
    #     if form.is_valid():
    #         form.save() # versi 1
    #         # models.EventsForm(**form.cleaned_data).save() # versi 2
    #         form = EventsForm()

    #     if guestform.is_valid():
    #         guestform.save() # versi 1
    #         # models.EventsForm(**form.cleaned_data).save() # versi 2
    #         form = EventsGuestForm()

    # else:
    #     form = EventsForm()
    #     guestform = EventGuestsForm()

    # model = Events.objects.all()
    # guests = EventGuests.objects.all()

    # return render(request, 'eventpage-templates/events.html', {'form': form, 'model' : model, 'guestform' : guestform, 'guests' : guests})