from django.test import TestCase, override_settings, Client
from django.urls import resolve
from django.http.request import HttpRequest
from django.shortcuts import render, redirect, get_object_or_404
from django.urls import reverse
from .apps import EventpageConfig
from .views import events
from .models import Events, EventGuests


# Create your tests here.
@override_settings(STATICFILES_STORAGE='django.contrib.staticfiles.storage.StaticFilesStorage')
class EventTest(TestCase):
    def createDb(self):
        self.client = Client()
        self.event = Events.objects.create(nama_event='mabumabu', deskripsi='asikasikan')
        self.guest = EventGuests.objects.create(event_host = self.event, guest='sarmiji')

    def test_event_page_using_view_func(self):
        found = resolve('/event/')
        self.assertEqual(found.func, events)

    def test_event_page_is_exist(self):
        response = self.client.get('/event/')
        self.assertEqual(response.status_code, 200)

    def test_event_page_post_success_and_render_the_result(self):
        response_post = self.client.post(
            '/event/', {
                'nama_event': 'Mabok',
                'deskripsi': 'asikasikan'
            }
        )
        self.assertEqual(response_post.status_code, 200)

    def test_event_add_guest_page_post_success_and_render_the_result(self):
        response_post = self.client.post(
            '/event/add/Narkoba', {
                'guest': 'badingsyalala'
            }
        )
        self.assertEqual(response_post.status_code, 404)

    def test_apps(self):
        self.assertEqual(EventpageConfig.name, 'eventpage')
        # self.assertEqual(eventpage.get_app_config('eventpage').name, 'eventpage')


    # def test_event_page_content_is_written(self):
    #     self.assertIsNotNone(events)
    #     self.assertTrue(len(events) >= 30)

    # def test_event_page_is_completed(self):
    #     request = HttpRequest()
    #     response = events(request)
    #     html_response = response.content.decode('utf8')
    #     self.assertIn()