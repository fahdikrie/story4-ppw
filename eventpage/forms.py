from django import forms
from . import models

class EventsForm(forms.ModelForm):
    nama_event = forms.CharField(max_length=50, widget=forms.TextInput(
        attrs={
            'class' : 'form-control mx-auto',
            'placeholder' : "write the name of the event here...",
            'style' : 'width: 50%;'
        }, 
    ))


    deskripsi = forms.CharField(max_length=250, widget=forms.Textarea(
        attrs={
            'class' : 'form-control mx-auto',
            'placeholder' : "describe the event here...",
            'style' : 'width: 50%; height: 10vh; overflow: auto;'
        }, 
    ))

    class Meta:
        model = models.Events
        fields = (
            'nama_event',
            'deskripsi',
            )

class EventGuestsForm(forms.ModelForm):
    guest = forms.CharField(max_length=50, widget=forms.TextInput(
        attrs={
            'class' : 'form-control mx-auto',
            'placeholder' : "kindly write down your name here...",
            'style' : 'width: 50%;'
        }, 
    ))

    class Meta:
        model = models.EventGuests
        fields = (
            'guest',
            )