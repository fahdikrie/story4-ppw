from django.apps import AppConfig


class SchedulepageConfig(AppConfig):
    name = 'schedulepage'
