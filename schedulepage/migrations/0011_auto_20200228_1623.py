# Generated by Django 3.0.3 on 2020-02-28 09:23

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('schedulepage', '0010_auto_20200228_1517'),
    ]

    operations = [
        migrations.AlterField(
            model_name='academicschedule',
            name='subject_slug',
            field=models.SlugField(max_length=200, unique=True),
        ),
    ]
