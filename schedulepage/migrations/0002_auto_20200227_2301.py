# Generated by Django 3.0.3 on 2020-02-27 16:01

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('schedulepage', '0001_initial'),
    ]

    operations = [
        migrations.AlterField(
            model_name='academicschedule',
            name='academic_year',
            field=models.CharField(choices=[('2019/2020', '2019/2020'), ('2020/2021', '2020/2021'), ('2021/2022', '2021/2022'), ('2022/2023', '2022/2023')], default='2019/2020', max_length=10),
        ),
        migrations.AlterField(
            model_name='academicschedule',
            name='subject',
            field=models.CharField(max_length=40, unique=True),
        ),
    ]
