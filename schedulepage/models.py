from django.db import models
from django.utils.text import slugify

# Create your models here.
# bikin class model jadwal kuliah yg isinya:
# > NAMA MATKUL
# > DOSEN
# > SKS
# > DESKRIPSI MATKUL (matkul ttg apa kaliya)
# > SEMESTER DAN TA (GASAL 2019/2020)
# > RUANG KELAS

sks_choices = (
    (0, '0'),
    (1, '1'),
    (2, '2'),
    (3, '3'),
    (4, '4'),
    (5, '5'),
    (6, '6'),
    (7, '7'),
    (8, '8'),
    (9, '9'),
    (10, '10')
    )

semester_choices = (
    ('Odd Semester', 'Odd Semester'),
    ('Even Semester', 'Even Semester')
)

academic_year_choices = (
    ("2019/2020", '2019/2020'),
    ("2020/2021", '2020/2021'),
    ("2021/2022", '2021/2022'),
    ("2022/2023", '2022/2023')
)

class AcademicSchedule(models.Model):
    subject = models.CharField(max_length=200, unique=True)
    subject_slug = models.SlugField(max_length=200, unique=True)
    # subject_slug = slugify(subject)
    description = models.CharField(max_length=200)
    lecturer = models.CharField(max_length=40)
    course_credits = models.IntegerField(choices=sks_choices, default="0")
    semester = models.CharField(choices=semester_choices, max_length=20, default="Even Semester")
    academic_year = models.CharField(choices=academic_year_choices, default="2019/2020", max_length=10)
    classroom = models.CharField(max_length=20)

    def save(self, *args, **kwargs):
        self.subject_slug = slugify(self.subject)
        super(AcademicSchedule, self).save(*args, **kwargs)

    def __str__(self):
        return "subject = {}, description = {}, lecturer = {}, course credits = {}".format(self.subject, self.description, self.lecturer, self.course_credits)

    # def save(self, *args, **kwargs):
    #     self.subject_slug = slugify(self.subject)
    #     super(AcademicSchedule, self).save(*args, **kwargs)

    # def _generate_unique_slug(self):
    #     unique_slug = slugify(self.name)
    #     num = 1
    #     while MyModel.objects.filter(slug=unique_slug).exists():
    #         slug = '{}-{}'.format(unique_slug, num)
    #         num += 1
    #         return unique_slug

    # def save(self, *args, **kwargs):
    #     if not self.slug:
    #         self.slug = self._get_unique_slug()
    #     super().save(*args, **kwargs)

# bikin class model buat tugas spesifik satu subject (pake foreignkey)
# > NAMA TUGAS
# > SUBJECT
# > DEADLINE

class AcademicAssignment(models.Model):
    assignment_subject = models.ForeignKey(AcademicSchedule, on_delete= models.CASCADE, related_name='assignments_per_subject')
    assignment = models.CharField(max_length=40, unique=True)
    deadline = models.DateTimeField()

    def __str__(self):
        return "{}".format(self.assignment)

    class Meta:
        ordering = ['assignment_subject']


# bikin class model buat appointment yg isinya:
# > NAMA KEGIATAN
# > DESC
# > SUBJECTS
# > KAPAN
# > DIMANA

class RegularSchedule(models.Model):
    activity = models.CharField(max_length=40, unique=True)
    activity_desc = models.CharField(max_length=200)
    people = models.CharField(max_length=200)
    location = models.CharField(max_length=40)


    def __str__(self):
        return "activity = {}, description = {}, people = {}, location = {}".format(self.activity, self.activity_desc, self.people, self.location)

