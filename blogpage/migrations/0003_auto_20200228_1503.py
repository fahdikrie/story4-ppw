# Generated by Django 3.0.3 on 2020-02-28 08:03

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('blogpage', '0002_auto_20200228_1501'),
    ]

    operations = [
        migrations.AlterField(
            model_name='post',
            name='slug',
            field=models.SlugField(default='HAHA', max_length=200),
        ),
    ]
