# Generated by Django 3.0.3 on 2020-02-28 08:01

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('blogpage', '0001_initial'),
    ]

    operations = [
        migrations.AlterField(
            model_name='post',
            name='slug',
            field=models.SlugField(default=None, max_length=200, unique=True),
        ),
    ]
